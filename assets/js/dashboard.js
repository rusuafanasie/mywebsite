import $ from 'jquery';

const dashboard = $('#dashboard');
const cards = dashboard.find('.row > div');
const firstCard = cards[0];
const lastCard = cards[cards.length - 1];

var middleView = window.innerWidth / 2;
var currentCursorX = middleView;
var currentTranslateX = 0;
const limit = 7;
const rotateYLimit = 35;

const left = 1;
const right = -1;

dashboard.mousemove(function (e) {
    currentCursorX = e.clientX;
    middleView = this.clientWidth / 2;
});

/*setInterval(dashboardMenuMove,20);*/

function dashboardMenuMove() {

    if (verifyMiddleLimit(firstCard, left) || verifyMiddleLimit(lastCard, right)) return window.requestAnimationFrame(dashboardMenuMove);

    const deadZoneRight = percentageValueScreenCalculator(20,right);
    const deadZoneLeft = percentageValueScreenCalculator(20,left);

    if (middleView < currentCursorX && currentCursorX > deadZoneRight) {
        const speed = movementSpeedResultPx(deadZoneRight, right);
        currentTranslateX = currentTranslateX - (speed > limit ? limit : speed);

    } else if (middleView > currentCursorX && currentCursorX < deadZoneLeft) {
        const speed = movementSpeedResultPx(deadZoneLeft, left);
        currentTranslateX = currentTranslateX + (speed > limit ? limit : speed);
    }

    /*const yRotate = (rotateYLimit * currentTranslateX)/middleView;*/

    cards.each((i, card) => {

            const rotationYDeg = yRotationResultPx(card);
            card.style.transform = 'translateX(' + (currentTranslateX) + 'px) rotateY(' + rotationYDeg + 'deg)';
    });


    return window.requestAnimationFrame(dashboardMenuMove);
}

function percentageValueScreenCalculator(percent, middleCursorPosition) {
    if (middleCursorPosition === left) {
        const firstPart = middleView * percent / 100;
        return  middleView - firstPart;
    } else if (middleCursorPosition === right) {
        const firstPart = middleView * percent / 100;
        return middleView + firstPart;
    }
}

function movementSpeedResultPx(deadZone, middleCursorPosition) {
    return middleCursorPosition === right ?
        0.0125 * currentCursorX - 1.25 * deadZone/100:
        -0.0125 * currentCursorX + 1.25 * deadZone/100;
}

function yRotationResultPx(element) {
    const elementCenterPx = element.clientWidth / 2;
    const elementDetails = element.getBoundingClientRect();

    const elementMiddlePosition = elementDetails.left + elementCenterPx;


    if (middleView < elementMiddlePosition) {
        const viewElementDifference = elementMiddlePosition - middleView;
        const elementPositionPercent = (100 * viewElementDifference)/middleView;

        return (rotateYLimit * elementPositionPercent)/100;
    } else {
        const elementPositionPercent = 100 - (100 * elementMiddlePosition)/middleView;

        return -(rotateYLimit * elementPositionPercent)/100;
    }
}

function verifyMiddleLimit(element, limit) {
    const elementCenterPx = element.clientWidth / 2;
    const elementDetails = element.getBoundingClientRect();

    const elementMiddlePosition = elementDetails.left + elementCenterPx;

    if (limit === left) return elementMiddlePosition >= middleView && currentCursorX <= middleView;
    else return elementMiddlePosition <= middleView && currentCursorX >= middleView;

}

dashboardMenuMove();